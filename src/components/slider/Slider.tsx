import React, { useState, useEffect } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { EffectCoverflow } from "swiper";
import { default as NextImage } from "next/image";

// Import Swiper styles
import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/pagination";
import "swiper/css/navigation";

export default function Slider() {
  const [swiperProps, setSwiperProps] = useState({
    slidesPerView: 5,
    spaceBetween: 80,
    coverflowEffect: {
      rotate: 0,
      stretch: 0,
      depth: 80,
      modifier: 1,
    },
  });

  useEffect(() => {
    function handleResize() {
      const screenWidth = window.innerWidth;

      if (screenWidth < 768) {
        setSwiperProps({
          slidesPerView: 5,
          spaceBetween: 0,
          coverflowEffect: {
            rotate: 0,
            stretch: 0,
            depth: 100,
            modifier: 1,
          },
        });
      } else if (screenWidth < 992) {
        setSwiperProps({
          slidesPerView: 5,
          spaceBetween: 10,
          coverflowEffect: {
            rotate: 0,
            stretch: 0,
            depth: 100,
            modifier: 1,
          },
        });
      } else {
        setSwiperProps({
          slidesPerView: 5,
          spaceBetween: 80,
          coverflowEffect: {
            rotate: 0,
            stretch: 0,
            depth: 80,
            modifier: 1,
          },
        });
      }
    }
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <>
      <Swiper
        centeredSlides={true}
        updateOnWindowResize={true}
        navigation={false}
        modules={[EffectCoverflow]}
        className="mySwiper"
        {...swiperProps}
        effect={"coverflow"}
        coverflowEffect={swiperProps.coverflowEffect}
      >
        <SwiperSlide>
          <div className="slider-element">
            <NextImage
              src="./images/slider1.svg"
              alt="title"
              width={464}
              height={464}
              priority
            />
            <p className="slider-album">Album 1</p>
            <p className="slider-published">Publicado: 01-01-2000</p>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="slider-element">
            <NextImage
              src="./images/slider2.svg"
              alt="title"
              width={464}
              height={464}
              priority
            />
            <p className="slider-album">Album 2</p>
            <p className="slider-published">Publicado: 01-01-2000</p>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="slider-element">
            <NextImage
              src="./images/slider3.svg"
              alt="title"
              width={464}
              height={464}
              priority
            />
            <p className="slider-album">Album 3</p>
            <p className="slider-published">Publicado: 01-01-2000</p>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="slider-element">
            <NextImage
              src="./images/slider4.svg"
              alt="title"
              width={464}
              height={464}
              priority
            />
            <p className="slider-album">Album 4</p>
            <p className="slider-published">Publicado: 01-01-2000</p>
          </div>
        </SwiperSlide>
      </Swiper>
    </>
  );
}
