import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";

import * as yup from "yup";

const schema = yup
  .object({
    name: yup
      .string()
      .required("Este campo es requerido")
      .matches(/^[a-zA-Z]+$/, "Solo se admiten letras"),
    email: yup
      .string()
      .required("Este campo es requerido")
      .email("Coloque un email valido"),
    reason: yup
      .string()
      .required("Este campo es requerido")
      .matches(/^[a-zA-Z]+$/, "Solo se admiten letras"),
    description: yup
      .string()
      .required("Este campo es requerido")
      .matches(/^[a-zA-Z]+$/, "Solo se admiten letras"),

    //select_dni: yup.string().required("Este campo es requerido"),
  })
  .required();

export default function Form() {
  const [activeName, setActiveName] = useState(false);
  const [activeEmail, setActiveEmail] = useState(false);
  const [activeReason, setActiveReason] = useState(false);
  const [activeDescription, setActiveDescription] = useState(false);
  // const [activeName, setActiveName] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm({
    mode: "onChange",
    resolver: yupResolver(schema),
    defaultValues: {
      name: "",
      email: "",
      reason: "Item2",
      description: "",
      condition: "",
    },
  });

  const onSubmit = (data: any) => {
    console.log(data);
  };

  return (
    <>
      <form
        className="form-music"
        autoComplete="off"
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className="content-form-music">
          <div className="form-group">
            <div className="form-group-input-content">
              <input
                className="input-form-music-name"
                type="text"
                placeholder={"Nombre"}
                onClick={(e) => {
                  setActiveName(true);
                  setActiveEmail(false);
                  setActiveReason(false);
                }}
                {...register("name")}
              />
              <span className={`${activeName ? "active" : "inactive"}`}></span>
            </div>

            {errors.name?.message && <p>{errors.name?.message}</p>}
          </div>

          <div className="form-group">
            <div className="form-group-input-content">
              <input
                className="input-form-music-email"
                type="text"
                placeholder={"Email"}
                onClick={(e) => {
                  setActiveEmail(true);
                  setActiveName(false);
                  setActiveReason(false);
                  setActiveDescription(false);
                }}
                {...register("email")}
              />
              <span className={`${activeEmail ? "active" : "inactive"}`}></span>
            </div>

            {errors.name?.message && <p>{errors.name?.message}</p>}
          </div>

          <div className="form-group">
            <div className="form-group-input-content">
              <select
                onClick={(e) => {
                  setActiveEmail(false);
                  setActiveName(false);
                  setActiveReason(true);
                  setActiveDescription(false);
                }}
                className="select-form-music-reason"
                {...register("reason")}
                // defaultValue={"Item2"}
                defaultValue={"Item2"}
              >
                <option disabled value="Item2">
                  Razon
                </option>
                <option value="Item2">Item2</option>
                <option value="Iten1">Iten3</option>
                <option value="Iten1">Iten4</option>
              </select>
              <span
                className={`${activeReason ? "active" : "inactive"}`}
              ></span>
            </div>

            {errors.name?.message && <p>{errors.name?.message}</p>}
          </div>
          <div className="form-group">
            <div className="form-group-input-content">
              <input
                className="input-form-music-description"
                type="text"
                placeholder={"description"}
                onClick={(e) => {
                  setActiveName(false);
                  setActiveEmail(false);
                  setActiveReason(false);
                  setActiveDescription(true);
                }}
                {...register("description")}
              />
              <span
                className={`${activeDescription ? "active" : "inactive"}`}
              ></span>
            </div>
          </div>
        </div>
        <div className="actions">
          {true ? (
            <button
              className={isValid ? "accept-button" : "disabled-button"}
              type="submit"
              disabled={isValid ? false : true}
            >
              Enviar
            </button>
          ) : null}
        </div>
      </form>
    </>
  );
}
