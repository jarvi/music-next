import Slider from "../components/slider/Slider";
import Form from "@/components/form/Form";
import React from "react";
import { default as NextImage } from "next/image";

export default function Layout() {
  function scrollToElement(id: any) {
    const element = document.getElementById(id);
    if (element) {
      element.scrollIntoView({ behavior: "smooth" });
    }
  }

  return (
    <>
      <div className="layout">
        <section className="first-row">
          <div className="first-row-content">
            <div className="first-row-title">
              <div className="first-row-title-content">
                <span className="first-row-title-music-span"> Music </span>
              </div>
            </div>
            <div className="content-arrow-text">
              <div className="img-svg">
                <div className="img-svg-content">
                  <NextImage
                    src="./flecha.svg"
                    alt="title"
                    width={464}
                    height={464}
                    className="img-flecha"
                    priority
                  />
                </div>
              </div>

              <div className="information-box">
                <div className="information-box-content">
                  <span className="information-box_span1">Disfruta de la</span>
                  <span className="information-box_span2">mejor música </span>

                  <p>Accede a tu cuenta para guardar tus albumes favoritos.</p>
                  <div
                    onClick={() => {
                      scrollToElement("fourth-row");
                    }}
                  >
                    <span className="contact">Contacta</span>
                    <span className="contact-arrow">{`    ->`}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className="second-row">
          <div className="img-svg">
            <NextImage
              src="./imgbarra.svg"
              alt="title"
              width={1510}
              height={425}
              className="img-barra"
              priority
            />
          </div>

          <div className="information-box">
            <div className="information-box-content">
              <span className="information-box_span1">Los mas escuchados</span>
              <span className="information-box_span2">
                Disfruta de tu música a un solo click y descube que discos has
                guardado dentro de “mis álbumes”
              </span>
            </div>
          </div>
        </section>

        <section className="third-row">
          <Slider />
          <div className="slider-mobile">
            <div className="slider-element">
              <NextImage
                src="./images/slider1.svg"
                alt="title"
                width={1510}
                height={425}
                className="img-barra"
                priority
              />

              <p className="slider-album">Album 1</p>
              <p className="slider-published">Publicado: 01-01-2000</p>
            </div>
          </div>
        </section>

        <section id="fourth-row" className="fourth-row">
          <div className="fourth-row-content">
            <div className="content-arrow-text">
              <div className="information-box">
                <div className="information-box-content">
                  <span className="information-box_span1">Disfruta de la</span>
                  <span className="information-box_span2">mejor música </span>

                  <p>
                    Escríbenos en el siguiente formulario y un asesor se pondrá
                    en contacto contigo.
                  </p>
                </div>
              </div>

              <Form />
            </div>
          </div>
        </section>
      </div>
    </>
  );
}
